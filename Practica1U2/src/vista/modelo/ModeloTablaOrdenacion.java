/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista.modelo;

import controlador.ed.lista.ListaEnlazada;
import javax.swing.table.AbstractTableModel;

;

/**
 *
 * @author alyce
 */
public class ModeloTablaOrdenacion extends AbstractTableModel {

    private ListaEnlazada<Integer> lista = new ListaEnlazada<>();

    public ListaEnlazada<Integer> getLista() {
        return lista;
    }

    public void setLista(ListaEnlazada<Integer> lista) {
        this.lista = lista;
    }

    

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public int getRowCount() {
        return lista.size();
    }

    @Override
    public String getValueAt(int i, int i1) {
        Integer s = null;

        try {
            s = lista.get(i);
        } catch (Exception e) {
        }
        switch (i1) {

            case 0:
                return (s != null) ? s.getClass().getName() : "NO DEFINIDO";
            case 1:
                return (s != null) ? s.toString() : "NO DEFINIDO";

            default:
                return null;
        }
    }

    @Override
    public String getColumnName(int column) {

        switch (column) {
            case 0:
                return "CLASE";
            case 1:
                return "NumerosRandom";

            default:
                return null;
        }
    }

}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.ordenacion;

import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.NodoLista;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.VacioException;

/**
 *
 * @author mt3k
 */
public class Quicksort<E> {

    public <E> ListaEnlazada<E> quicksort(ListaEnlazada<E> lista, boolean ordenacion) throws VacioException, PosicionException {
        quicksortRecursivo(lista, 0, lista.getSize() - 1, ordenacion);
        return lista;
    }

    private <E> void quicksortRecursivo(ListaEnlazada<E> lista, int inicio, int fin, boolean ascendente) throws VacioException, PosicionException {
        if (inicio < fin) {
            int indicePivote = particion(lista, inicio, fin, ascendente);
            quicksortRecursivo(lista, inicio, indicePivote - 1, ascendente);
            quicksortRecursivo(lista, indicePivote + 1, fin, ascendente);
        }
    }

    private <E> int particion(ListaEnlazada<E> lista, int inicio, int fin, boolean ascendente) throws VacioException, PosicionException {
        E pivote = lista.get(fin);
        int i = inicio - 1;

        for (int j = inicio; j < fin; j++) {
            E elementoActual = lista.get(j);
            if ((ascendente && compararElementos(elementoActual, pivote) <= 0)
                    || (!ascendente && compararElementos(elementoActual, pivote) >= 0)) {
                i++;
                lista.intercambioElementos(i, j);
            }
        }

        lista.intercambioElementos(i + 1, fin);
        return i + 1;
    }

    private <E> int compararElementos(E elemento1, E elemento2) {
        if (elemento1 instanceof String && elemento2 instanceof String) {
            String str1 = (String) elemento1;
            String str2 = (String) elemento2;
            return str1.compareTo(str2);
        } else if (elemento1 instanceof Integer && elemento2 instanceof Integer) {
            Integer int1 = (Integer) elemento1;
            Integer int2 = (Integer) elemento2;
            return int1.compareTo(int2);
        }
        return 0;
    }
}

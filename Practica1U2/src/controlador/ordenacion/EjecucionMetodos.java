/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.ordenacion;

import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.NodoLista;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.VacioException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mt3k
 */
public class EjecucionMetodos {

    public void imprimir(ListaEnlazada<Integer> m) {
        System.out.println("IMPRIMIR MATRIZ");
        for (int i = 0; i < m.size(); i++) {
            try {
                System.out.print(m.get(i).toString() + " ,");
            } catch (VacioException ex) {
                Logger.getLogger(EjecucionMetodos.class.getName()).log(Level.SEVERE, null, ex);
            } catch (PosicionException ex) {
                Logger.getLogger(EjecucionMetodos.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("\n FIN---------------");
    }

    public void imprimirS(ListaEnlazada<String> m) {
        System.out.println("IMPRIMIR MATRIZ");
        for (int i = 0; i < m.size(); i++) {
            try {
                System.out.print(m.get(i) + " ,");
            } catch (VacioException ex) {
                Logger.getLogger(EjecucionMetodos.class.getName()).log(Level.SEVERE, null, ex);
            } catch (PosicionException ex) {
                Logger.getLogger(EjecucionMetodos.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("\n FIN---------------");
    }

    public static ListaEnlazada<Integer> generarListaEnlazada(int tamaño) {
        ListaEnlazada<Integer> lista = new ListaEnlazada<>();
        Random random = new Random();

        for (int i = 0; i < tamaño; i++) {
            lista.insertar(random.nextInt(1000)); // 
        }

        return lista;
    }

    public static void main(String[] args) throws VacioException, PosicionException {
        EjecucionMetodos em = new EjecucionMetodos();
//        ListaEnlazada<String> lista = new ListaEnlazada<>();
        Quicksort quicksort = new Quicksort();
        Merge merge = new Merge();
////        lista.insertar("HOLA");
////        lista.insertar("COMO ESTAS");
////        lista.insertar("GUANCHOPE");
////        lista.insertar("HI");
////        lista.insertar("LOLEADO");
////        lista.insertar("JAJAJAJAJA");
////        lista.insertar("AALABA");
////        lista.insertar("HOLA");
////        lista.insertar("A");
////        lista.insertar("C");
////        lista.insertar("Z");
////        lista.insertar("X");
////        lista.insertar("Y");
////        System.out.println("LISTA ORIGINAL:  ");
////        em.imprimirS(lista);
////        ListaEnlazada<String> listaOrdenadaAscendente = merge.mergeSort(lista, true);
////        System.out.println("LISTA ordenada:  ");
////        em.imprimirS(listaOrdenadaAscendente);

        ListaEnlazada<Integer> pruebas = em.generarListaEnlazada(20000);
        em.imprimir(pruebas);
        long inicio = System.currentTimeMillis();
        ListaEnlazada<Integer> pruebas1 = merge.mergeSort(pruebas, true);
        long fin = System.currentTimeMillis();
        long totalTiempo = fin - inicio;
        em.imprimir(pruebas1);
        System.out.println("Tiempo de ejecución de MergeSort: " + totalTiempo + " ms");
        ListaEnlazada<Integer> pruebas3 = em.generarListaEnlazada(20000);
        em.imprimir(pruebas3);
        long inicio1 = System.currentTimeMillis();
        ListaEnlazada<Integer> pruebas4 = merge.mergeSort(pruebas3, true);
        long fin1 = System.currentTimeMillis();
        long totalTiempo1 = fin1 - inicio1;
        em.imprimir(pruebas4);
        System.out.println("Tiempo de ejecución de Quicksort: " + totalTiempo1 + " ms");
    }

}
